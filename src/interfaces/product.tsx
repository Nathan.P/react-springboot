export interface ProductType {
  id: number;
  nom: String;
  prix: number;
  prixAchat: number;
  status: boolean;
}