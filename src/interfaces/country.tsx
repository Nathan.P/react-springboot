export interface CountryType {
  name: string;
  capital: string;
  region: string;
}