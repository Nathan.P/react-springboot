import Countries from "../components/Countries";
import CountrySearchByName from "../components/CountrySearchByName";

const CountriesPage = () => {
  
  return (
    <div className="space-y-6 p-5">
      <CountrySearchByName />
      <Countries />
    </div>
  );
};

export default CountriesPage;
