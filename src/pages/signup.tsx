import SignUp from "../components/SignUp";

const SignUpPage = () => {
  return (
    <div className="signup">
      <SignUp />
    </div>
  );
};

export default SignUpPage;