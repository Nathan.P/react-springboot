import Products from "../components/Products";

const ProductsPage = () => {
  
  return (
    <div className="products">
      <Products />
    </div>
  );
};

export default ProductsPage;
