import { useCallback, useEffect, useState } from "react";
import apiCall from "../helpers/apiCall";
import { ProductType } from "../interfaces/product";

const Countries = () => {
  const [userProducts, setUserProducts] = useState<ProductType[] | []>([]);
  const [products, setProducts] = useState<ProductType[] | []>([]);

  const toggleProduct = async (id: number) => {
    const productsIds: number[] = [];

    products.forEach((product) => {
      if (product.id === id) product.status = !product.status;
      if (product.status === true) productsIds.push(product.id);
    });

    await apiCall(
      "PUT",
      "http://localhost:9080/api/users/products/set",
      JSON.stringify(productsIds),
      true
    );
    setUserProducts(
      await apiCall("GET", "http://localhost:9080/api/users/products")
    );
  };

  const defineProductsStatus = useCallback(() => {
    const productsClone = [];
    for (const product of products) {
      const productClone = product;
      productClone.status = false;
      for (const userProduct of userProducts) {
        if (userProduct.id === product.id) productClone.status = true;
      }
      productsClone.push(productClone);
    }
    setProducts(productsClone);
  }, [products, userProducts]);

  useEffect(() => {
    (async () => {
      setProducts(await apiCall("GET", "http://localhost:9080/api/products"));
      setUserProducts(
        await apiCall("GET", "http://localhost:9080/api/users/products")
      );
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    defineProductsStatus();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userProducts]);

  return (
    <div className="countries">
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Id
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Name
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Prix
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Prix Achat
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Status
                    </th>
                    <th scope="col" className="relative px-6 py-3">
                      <span className="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {products !== [] &&
                    products.map((product, index: number) => (
                      <tr key={index}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {product.id}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {product.nom}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {product.prix}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {product.prixAchat}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {product.status ? (
                            <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                              added
                            </span>
                          ) : (
                            <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                              not added
                            </span>
                          )}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <button onClick={() => toggleProduct(product.id)}>
                            {product.status ? "Remove" : "Add"}
                          </button>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Countries;
