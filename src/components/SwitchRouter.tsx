import { useContext } from "react";
import { Switch, Route } from "react-router-dom";

import { store } from "../store/store";

import Countries from "../pages/countries";
import Home from "../pages/home";
import Login from "../pages/login";
import Products from "../pages/products";
import SignUp from "../pages/signup";

const SwitchRouter = () => {
  const {
    state: { token },
  } = useContext(store);

  return (
    <Switch>
      <Route path="/login">{!token && <Login />}</Route>
      <Route path="/signup">{!token && <SignUp />}</Route>
      <Route path="/products">{token && <Products />}</Route>
      <Route path="/countries">
        <Countries />
      </Route>
      <Route path="/">
        <Home />
      </Route>
    </Switch>
  );
};

export default SwitchRouter;
