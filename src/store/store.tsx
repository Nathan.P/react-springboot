import React, { createContext, useReducer } from "react";
import getToken from "../helpers/token/getToken";
import removeToken from "../helpers/token/removeToken";
import saveToken from "../helpers/token/saveToken";

import { Actions, ActionType } from "./store.actions";

interface IStoreState {
  token: string | null;
}

interface IAppContext {
  state: IStoreState;
  dispatch: React.Dispatch<Actions>;
}

const initialState: IStoreState = {
  token: getToken()
};

const store = createContext<IAppContext>({
  state: initialState,
  dispatch: () => null
});

const { Provider } = store;

const reducer = (state: IStoreState, action: Actions) => {
  switch (action.type) {
    case ActionType.RemoveToken:
      removeToken()
      return { token: null };
    case ActionType.SetToken:
      saveToken(action.payload)
      return { token: action.payload };
    default:
      return state;
  }
};

const AppProvider = ({ children }: { children: JSX.Element }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, AppProvider };