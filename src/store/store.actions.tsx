export enum ActionType {
  RemoveToken = "auth/removetoken",
  SetToken = "auth/settoken",
}

interface IRemoveToken {
  type: ActionType.RemoveToken;
}

interface ISetToken {
  type: ActionType.SetToken;
  payload: string;
}

export type Actions = IRemoveToken | ISetToken;

export const RemoveToken = (): IRemoveToken => ({
  type: ActionType.RemoveToken,
});

export const SetToken = (value: string): ISetToken => ({
  type: ActionType.SetToken,
  payload: value,
});
