import getToken from "./token/getToken";

const apiCall = async (
  method: string,
  url: string,
  body?: RequestInit["body"],
  json?: boolean
) => {
  const token = getToken();

  if (!url || !method) return;

  const headers: HeadersInit = token
    ? {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      }
    : { "Content-Type": "application/json" };

  const res = await fetch(url, {
    method,
    headers,
    body,
  });
  if (json) return res;
  return res.json();
};

export default apiCall;
