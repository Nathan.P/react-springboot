const removeToken = () => {
  sessionStorage.removeItem("token");
};

export default removeToken;