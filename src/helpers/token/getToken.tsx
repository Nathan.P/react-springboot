const getToken = () => {
  const tokenString = sessionStorage.getItem("token");
  return tokenString ? JSON.parse(tokenString) : null;
};

export default getToken;