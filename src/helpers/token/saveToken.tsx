const saveToken = (token: string ) => {
  sessionStorage.setItem("token", JSON.stringify(token));
};

export default saveToken;