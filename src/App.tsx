import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";

import Nav from "./components/Nav";
import { AppProvider } from "./store/store";
import SwitchRouter from "./components/SwitchRouter";

function App() {
  return (
    <div className="App">
      <AppProvider>
        <Router>
          <div>
            <Nav />
            <SwitchRouter />
          </div>
        </Router>
      </AppProvider>
    </div>
  );
}

export default App;
